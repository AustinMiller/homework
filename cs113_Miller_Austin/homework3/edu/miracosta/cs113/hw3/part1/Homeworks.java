package edu.miracosta.cs113.hw3.part1;
import java.util.*;

public class Homeworks{
	public static void main(String args[]){
		List<Assignment> myList = new ArrayList<Assignment>();
		
		String assign1 = "Assignment One";
		GregorianCalendar due1 = new GregorianCalendar(2015, 10, 23);
		
		Assignment pound1 = new Assignment(due1, assign1);
		
		myList.add(pound1);
		
		String assign2 = "Assignment Two";
		GregorianCalendar due2 = new GregorianCalendar(2015, 9, 20);
		
		Assignment pound2 = new Assignment(due2, assign2);
		
		myList.add(pound2);
		
		String assign3 = "Assignment Three";
		GregorianCalendar due3 = new GregorianCalendar(2014, 10, 24);
		
		Assignment pound3 = new Assignment(due3, assign3);
		
		myList.add(pound3);
		
		String assign4 = "Assignment Four";
		GregorianCalendar due4 = new GregorianCalendar(2015, 11, 23);
		
		Assignment pound4 = new Assignment(due4, assign4);
		
		myList.add(pound4);
		
		int listSize = myList.size();
		
		GregorianCalendar only = new GregorianCalendar();
		
		for(int i = 0; i < listSize; i++){
			if(myList.get(i).dueDate.compareTo(only)<0){
				myList.remove(i);
				i--;
				listSize = listSize-1;
			}
		}
		
		
		
		
		
		Assignment currentAssign = myList.get(0);
		
		List<Assignment> currentAssignments = new ArrayList<Assignment>();
		currentAssignments.add(currentAssign);
		
		for(int i = 1; i < listSize; i++){
			if(currentAssignments.get(0).dueDate.compareTo(myList.get(i).dueDate)>0){
				for(int j = 0; j < currentAssignments.size(); j++){
					currentAssignments.set(j, null);
				}
				currentAssignments.set(0,myList.get(i));
			}
			else if(currentAssign.dueDate.compareTo(myList.get(i).dueDate)==0){
				currentAssignments.set(1, myList.get(i));
			}
		}
		
		
		
		
		
		
		
		
		System.out.println(listSize);
		for(int i = 0; i <currentAssignments.size(); i++){
			System.out.println(currentAssignments.get(i).name);
		}
	}
}
