package edu.miracosta.cs113.hw1.part2;

import java.util.Arrays;

public class SameStuff {
	public static void main(String[] args) {
 		int[] a = {2,5,5,7,7,8,9};
		int[] b = {5,7,9,5,8,7,2};
		int[] c = {2,7,8,7,5,7,5,7};
		int[] d = {8,7,5,7,2,5,7,7};
		
		System.out.println("Arrays equal: " + sameElements(a,b));
		
	}
	
	public static boolean sameElements(int[] a, int[] b){
		int placeholder;
		
		Arrays.sort(a);
		Arrays.sort(b);
		
		printArray(a);
		printArray(b);
		
		if(a.length == b.length){
			for(int x=0; x<a.length; x++){
				if(a[x] != b[x]){
					return false;
				}
			}
			return true;
		}
		else{
			return false;
		}
	}
	
	public static void printArray(int[] a){
		for (int j = 0; j < a.length; j++) {
			System.out.print(a[j]+", ");
		}
		System.out.println();
	}
}
