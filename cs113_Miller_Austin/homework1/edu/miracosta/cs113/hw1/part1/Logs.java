package edu.miracosta.cs113.hw1.part1;

import java.math.*;

public class Logs {
	public static void main(String[] args) {
		for(double n=0; n <21; n++){
			double i = Math.pow(2,n)*1000;
			System.out.println(i+"\t"+Math.log(i)+"\t");
		}
	}
}
