package edu.miracosta.cs113.hw2.part3;

public class Things {
	public static void main(String args[]){
		int n = 10;
		
/*		for (int i = 0; i < n; i++){
			for (int j = 0; j < n; j++){
				System.out.println(i + " " + j); //o(n)
			}
		}
*/		
		System.out.println("part 2");
		
		for (int i = 0; i < n; i++){
			for (int j = 0; j < 2; j++){
				System.out.println(i + " " + j); //o(n)
			}
		}
		
		System.out.println("part 3");
		
		for (int i = 0; i < n; i++){
			for (int j = n - 1; j >= i; j--){
				System.out.println(i + " " + j); //o(n2)
			}
		}
		
		System.out.println("part 4");
		
		for (int i = 1; i < n; i++){
			for (int j = 0; j < i; j++){
				if (j % i == 0){
					System.out.println(i + " " + j); //o(n2)
				}
			}
		}
		
	}
}